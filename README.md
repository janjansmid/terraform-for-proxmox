# Terraform 

## What is it
- IaaC - Infrastructure as a Code
- [What is Terraform?](https://www.terraform.io/intro)
- [Terraform in 100 Seconds](https://www.youtube.com/watch?v=tomUWcQ0P3k)
- It's simply a tool for managing Infrastructure as a Code.
  - Used mainly in public cloud (aws, digitalocean, etc.).
  - Thanks to [telmate provider](https://github.com/Telmate/terraform-provider-proxmox) also usable in Proxmox.
  - Check proxmox-example.tf for example of terraform syntax.

## Installation

Debian Bullseye installation: [https://nextgentips.com/2021/11/20/how-to-install-terraform-on-debian-11/](https://nextgentips.com/2021/11/20/how-to-install-terraform-on-debian-11/)

## Cloud-init

- Terraform principle is based on creating **specified amount of virtual machines with specified parameters** using existing virtual machine **template** created upfront inside PVE.
- This template *can be created from any virtual machine* by adding support for cloud-init:
  - Install cloud-init package
    - For example. *apt install cloud-init*
  - Add *cloud-init drive* in Hardware section.
  
  - ![cloud-init-drive](images/cloud-init-drive.png)
- Cloud-init allows us to setup properties of operating system inside virtual machine directly from Proxmox or Terraform.
- Properties setup using cloud-init are available inside PVE in section called ... cloud-init.:
  - ![cloud-init](images/cloud-init.png)


## How to terraform

- Terraform works per directory, for example *test-vms*.
  - **Do not combine multiple projects in one directory unless you know what you are doing!**
  - For your own "project" copy existing directory (test-vms) and edit .tf files to your needs.

- Create or edit a *.tf* file (for example *main.tf*).
  - Dont forget to add password to your Proxmox VE in provider setup section on top of .tf file.
  - Check and edit contents of *vars.tf*.

- Run terraform:  
    1. **terraform init** - Initializes terraforms current working directory.
    2. **terraform plan** - List of changes to performe. It's a long read, but worth it to check it.
    3. **terraform apply** - Performs requested changes -> Creates VMs per specification.
    4. *terraform destroy* - Destroys everything from previous step, therefore removes all VMs.

![terraform](images/terraform-howto.png)

## Aliases

Useful aliases to shorten your commands *tf, tfi, tfp, tfa, tfp*:

 ```
 echo alias tf=\'terraform\' >> ~/.bashrc
 echo alias tfi=\'terraform init\' >> ~/.bashrc
 echo alias tfp=\'terraform plan\' >> ~/.bashrc
 echo alias tfa=\'terraform apply\' >> ~/.bashrc
 echo alias tfa1=\'terraform apply -parallelism=1\' >> ~/.bashrc
 echo alias tfd=\'terraform destroy\' >> ~/.bashrc
 source ~/.bashrc
 ```
