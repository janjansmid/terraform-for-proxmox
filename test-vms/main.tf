terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = ">=2.9.11"
    }
  }
}

provider "proxmox" {
  # Configuration options
  pm_api_url = "https://pve.domain.com:8006/api2/json"
  pm_user = "root@pam"
  pm_password = "SuperSecretPassword"
  pm_tls_insecure = "true"
}

#-----------------------------------------------------------------------
#-----------------------------------------------------------------------
resource "proxmox_vm_qemu" "test_server_1" {
  count = 2 
  name = "test-vm-${count.index + 1}"
  vmid = "30${count.index + 1}"
  target_node = element(var.pve_nodes, 0)
  clone = var.template_name
  agent = 1
  os_type = "cloud-init"
  cores = 1
  sockets = 1
  cpu = "host"
  memory = 1024
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"
  disk {
    cache = "writeback"
    iothread = 1
    discard = "on"
    ssd   = 1
    slot  = 0
    size  = var.storage_size
    storage = var.storage_target
    type  = "scsi"
  }
  
  # if you want two NICs, just copy this whole network section and duplicate it
  network {
    model = "virtio"
    bridge = "vmbr0"
    #tag = 3800
  }
  ssh_user = "root"
  sshkeys  = var.ssh_keys
    
  ipconfig0 = "ip=${var.test_network}.3${count.index + 1}/${var.test_network_prefix},gw=${var.test_network}.1"  

}
#-----------------------------------------------------------------------
#-----------------------------------------------------------------------
