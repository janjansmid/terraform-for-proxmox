variable "ssh_keys" {
  default = <<EOF
  ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDeyeF7mBIUy5hfd5JKkL+yGee+XZzwXorc972PibEGzEjZA1jQWP16IR+jprzwVuRiRQV2Dx3VgGgTLYZn6f9YXOtWdlc7dRyyWAHFyhUWEN7KLBJqGJbI403t296OdNPT5iRGdsicxVyK92DyPrk88WJmv2wlWRfRNeO0wycFA6rvPuElW8s6/CaGXDhUKlNVeg+NuiMcZln5Pmci6oRyk+nCv03gA4u8rLqcUhW0g04sWlK+X4W229HklRYUjDL8ZafZmoN3tcHyz2OwPDCOymOhY5wJw7oMZdPeNxUBBFuIv36Nty5u6uANvwlVic2qBlop/1k/Hr0Y72hL/5eT hans@Tachi
  ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjUcNQfGDK8/GcxRYVwFGThwToENuQEeZI0LZFotIavI3ch8R8DTy8wr496B8axmSKdU/ggCOdnCqU+B6b8vxjj9SLk2q47o1LTTKJKD4dnGrxCUDquudbjE87zeeF6FWeaOu94WZBsoHi4/7Y4HEEWYrub9EvGblRjkkNVHg44oaZlV5A3VksLJzUIsiNGM5hvp4he58PaSLqkseyC+1gqyCA7JAllkh/dci12dxgU5Aa2e8BoWVwKgp+KBo1vBmU7MoEagVZlpF7nLl9ECVI/+SUPL/WugrVs6AimlTQXH6Q83K9a4MVpsNmWQa4aZtl0E/OWFSaQILQOc6g+cQP hans@Roci
  EOF
}

variable "storage_target" {
  default = "local-ssd"
}

variable "storage_size" {
  default = "20G"
}

variable "template_name" {
  default = "server-template-cloud-init"
}

variable "test_network" {
  default = "192.168.10"
}

variable "test_network_prefix" {
  default = "24"
}

variable "pve_nodes" {
  type        = list(any)
  description = "available PVE nodes"
  default     = ["pve","pve1","pve02"]
}