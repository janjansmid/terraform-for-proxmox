terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = ">=2.9.11"
    }
  }
}

provider "proxmox" {
  # PVE server configuration options
  pm_api_url = "https://your.pve.server.url:8006/api2/json"
  pm_user = "root@pam"
  pm_password = "YourSuperSecretPassword"
  pm_tls_insecure = "true"
}

resource "proxmox_vm_qemu" "test_server_3" {
  count = 1 
  name = "test-vm-3" 
  target_node = var.proxmox_host
  clone = var.template_name
  agent = 1
  os_type = "cloud-init"
  cores = 3
  sockets = 1
  cpu = "host"
  memory = 3072
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"
  disk {
    slot = 0
    # make sure your storage exists and has sufficient size
    size = "30G"
    type = "scsi"
    storage = "local-lvm"
    iothread = 1
  }
  
  # copy this block to add more interfaces 
  network {
    model = "virtio"
    bridge = "vmbr0"
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }
  
  #increment ipconfigX when adding more interfaces
  ipconfig0 = "ip=192.168.10.30/24,gw=192.168.10.1"
  
  # SSH public keys set by variables. 
  sshkeys = <<EOF
  ${var.ssh_key}
  EOF
}

resource "proxmox_vm_qemu" "test_server_2" {
  count = 1 
  name = "test-vm-2" 
  target_node = var.proxmox_host
  clone = var.template_name
  agent = 1
  os_type = "cloud-init"
  cores = 2
  sockets = 1
  cpu = "host"
  memory = 2048
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"
  disk {
    slot = 0
    # make sure your storage exists and has sufficient size
    size = "20G"
    type = "scsi"
    storage = "local-lvm"
    iothread = 1
  }
  
  # copy this block to add more interfaces 
  network {
    model = "virtio"
    bridge = "vmbr0"
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }
  
  ipconfig0 = "ip=192.168.10.20/24,gw=192.168.10.1"
  
  # SSH public keys set by variables. 
  sshkeys = <<EOF
  ${var.ssh_key}
  EOF
}

resource "proxmox_vm_qemu" "test_server_1" {
  count = 1 
  name = "test-vm-1" 
  target_node = var.proxmox_host
  clone = var.template_name
  agent = 1
  os_type = "cloud-init"
  cores = 1
  sockets = 1
  cpu = "host"
  memory = 4096
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"
  disk {
    slot = 0
    # make sure your storage exists and has sufficient size
    size = "10G"
    type = "scsi"
    storage = "local-lvm"
    iothread = 1
  }
  
  # copy this block to add more interfaces 
  network {
    model = "virtio"
    bridge = "vmbr0"
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }
  
  ipconfig0 = "ip=192.168.10.10/24,gw=192.168.10.1"
  
  # SSH public keys set by variables. 
  sshkeys = <<EOF
  ${var.ssh_key}
  EOF
}
